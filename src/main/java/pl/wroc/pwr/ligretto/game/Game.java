package pl.wroc.pwr.ligretto.game;

import pl.wroc.pwr.ligretto.model.CardSet;
import pl.wroc.pwr.ligretto.model.Player;

public class Game {

	private CardSet cardSet;
	private Player[] players;
	
	public Game() {
		cardSet = new CardSet();
		players = new Player[4];
	}
	
	public static void main(String[] args) {
		Game game = new Game();
		
		Player player1 = new Player();
		Player player2 = new Player();
		Player player3 = new Player();
		
		game.addPlayer(player1);
		game.addPlayer(player2);
		game.addPlayer(player3);

		game.play();

	}
	
	public CardSet getCardSet() {
		return cardSet;
	}

	public Player[] getPlayers() {
		return players;
	}

	public void play() {
		System.out.println("Welcome to Ligretto!");
	}

	public void addPlayer(Player player) {
		players[0] = player;
		
	}
	
}
