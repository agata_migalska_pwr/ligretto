package pl.wroc.pwr.ligretto.game;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import pl.wroc.pwr.ligretto.model.Player;

public class GameTest {

	private Game game;
	
	@BeforeClass
	public static void setUpClass() {
		
	}
	
	@Before
	public void setUp() {
		game = new Game();
	}
	
	@After
	public void tearDown() {
		
	}
	
	@AfterClass
	public static void tearDownClass() {
		
	}
	
	@Test
	public void shouldAddPlayerToGame() {
	
		//given
		Player expectedPlayer = createTestPlayer();

		//when
		game.addPlayer(expectedPlayer);
		
		//then
		assertNotNull("Players are null", game.getPlayers());
		assertEquals("Incorrect number of players", 1, game.getPlayers().length);

		Player actualPlayer = game.getPlayers()[0];
		assertEquals("Actual player is not equal to expected player", expectedPlayer, actualPlayer);
	}
	
	@Test
	public void shouldAddFourPlayersToGame() {
		//given
		Player expectedPlayer1 = createTestPlayer();
		Player expectedPlayer2 = createTestPlayer();
		Player expectedPlayer3 = createTestPlayer();
		Player expectedPlayer4 = createTestPlayer();
		
		//when
		game.addPlayer(expectedPlayer1);
		game.addPlayer(expectedPlayer2);
		game.addPlayer(expectedPlayer3);
		game.addPlayer(expectedPlayer4);
		
		//then
		assertNotNull("Players are null", game.getPlayers());
		assertEquals(4, game.getPlayers().length);
	}
	
	private Player createTestPlayer() {
		return new Player();
	}

}
